#import <substrate.h>
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

%hook MCProfile
-(unsigned long long)targetDeviceType {
  return 1;
}
%end

// %hook MCInstaller
// -(int)_targetValidationStatusForProfile:(id)arg1 {
//   int ret = %orig;
//   NSLog(@"[profiledPatch] ret: %d, arg1: %@", ret, arg1);
//   return 0;
// }
// %end

// %ctor {
//   @autoreleasepool {
//     NSLog(@"[profiledPatch] Loaded!");
//   }
// }